#include <stdio.h>
#include <unistd.h>
#include <iostream>

//Egészre mutató mutatót visszadó függvény 
int *func(int a){
int *ptr2= &a;
std::cout<<*ptr2<<"\n";
return ptr2;}

//Egészet visszaadó és két egészet kapó függvényre mutató mutatót visszaadó, egészet kapó függvény
int seged(int a,int b){
int c=a+b;
return c;}


int (*func2(int a))(int a,int b){
int (*funcptr2)(int,int)=&seged;
return funcptr2;
}




int main(){



//Egész
int a=5;
int b=6;

//Egész referenciája 
int &ref = a;

//Egészre mutató muató
int *ptr = &a;

//Egészek tömbje
int tomb[5];

//Egész tömb refrenciája
int (&tombref)[5] = tomb;

//Egészre mutató mutatók tömbje
int *ptrtomb [5];



//Egészre mutató mutatót visszadó függvényre mutató muató
int* (*funcptr)(int);
funcptr = &(*func);

//Függvénymutató egy egészet visszaadó és két egészet kapó függvényre mutató mutatót visszaadó, egészet kapó függvényre
int (*(*funcptr3)(int))(int, int);
funcptr3 = &(*func2);

std::cout<<*ptr<<" "<<&ref<<"\n";
std::cout<<funcptr<<"\n";


func(a);


return 0;
}
