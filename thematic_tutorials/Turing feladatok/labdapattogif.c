#include <stdio.h>
#include <curses.h>
#include <unistd.h>

int main(void){  //voidnak nincs visszatérési értéke

WINDOW *ablak;  //pointer, amely az ablakra mutat
ablak = initscr (); //a curses ablakot inicializáljuk

int x = 0;
int y = 0;

int xnov = 1;
int ynov = 1;

int mx;
int my;

for (;;) {


	getmaxyx (ablak, my, mx); //kurzor koordináták bekerülnek az my,mx változóba

	mvprintw (y,x,"O"); //kiíratás, a formázott output a curses ablakba

	refresh ();

	usleep(100000); //pihen a program ennyi milisecig
	clear(); //ablaktakarítás

	x = x+xnov; //+1 a koordinátákhoz

	y = y+ynov;

	if(x>=mx-1){     //jobb oldal?
  xnov = xnov * -1;
}

    if (x<=0){      //bal oldal?
  xnov = xnov * -1;
}

    if (y<=0){     //teteje?
  ynov = ynov * -1;
}

    if(y>=my-1){   //alja?

  ynov = ynov * -1;
}

}

return 0;

}
