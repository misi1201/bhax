package Lauda.Junit_teszt;

import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BinTreeTest {

    @Test
    public void testMeanAndDeviationValuesWhenBinTreeRan() throws IOException {
        // Given
        double expectedMean = 2.75;
        double expectedDeviation = 0.9574271077563381;
        // When
        BinTree actual = new BinTree();
        actual.testMain();
        // Then
        assertEquals(expectedMean, actual.mean);
        assertEquals(expectedDeviation, actual.getDeviation());
    }
}
