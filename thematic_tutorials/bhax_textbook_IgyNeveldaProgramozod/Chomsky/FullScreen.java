package Chomsky2;

import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import javax.swing.JButton;
import javax.swing.JFrame;

public class FullScreen {
   public static void main(String[] args) {
       GraphicsEnvironment graphics = GraphicsEnvironment.getLocalGraphicsEnvironment();
       GraphicsDevice device = graphics.getDefaultScreenDevice();
       JFrame frame = new JFrame("Fullscreen");
       JButton button = new JButton("Click to Close!");
       frame.add(button);
       device.setFullScreenWindow(frame);
       button.addActionListener(e -> {
           frame.dispose();
       });
   }
}
