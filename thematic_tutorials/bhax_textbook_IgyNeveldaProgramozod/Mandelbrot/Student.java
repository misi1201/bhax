package Mandelbrot;

import java.util.ArrayList;
import java.util.List;

public class Student {

    private String name;
    private String neptunID;
    private List<Course> courses = new ArrayList<Course>();

    public Student(String name, String neptunID) {
        this.name = name;
        this.neptunID = neptunID;
    }

    public String getName() {
        return name;
    }

    public String getNeptunID() {
        return neptunID;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void addCourse(Course course) {
        if (course.getCapacity() > 0) {
            this.courses.add(course);
            course.addStudent(this);
        } else {
            System.out.println("Betelt");
        }
    }

    public void removeCourse(Course course) {
        this.courses.remove(course);
        course.removeStudent(this);
    }
}
