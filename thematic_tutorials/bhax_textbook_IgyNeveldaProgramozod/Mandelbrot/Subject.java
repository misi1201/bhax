package Mandelbrot;

public class Subject {

    private String name;
    private String subjectID;
    private Integer credit;
    private Lecturer responsibleLecturer;
    private SubjectType subjectType;

    public Subject(String name, String subjectID, Integer credit, Lecturer responsibleLecturer, SubjectType subjectType) {
        this.name = name;
        this.subjectID = subjectID;
        this.credit = credit;
        this.responsibleLecturer = responsibleLecturer;
        this.subjectType = subjectType;
    }

    public String getName() {
        return name;
    }

    public Lecturer getResponsibleLecturer() {
        return responsibleLecturer;
    }

    public String getSubjectID() {
        return subjectID;
    }

    public Integer getCredit() {
        return credit;
    }

    public SubjectType getSubjectType() {
        return subjectType;
    }
}
